package com.company;

import com.company.TASK1.Task1;
import com.company.TASK2.Task2;

public class Main {
    public static void main(String[] args) {
        new Task1().run();
        new Task2().run();
    }
}