package com.company.TASK1.model;

import java.util.Locale;

public class Bank {
    private String name;
    private Currency[] rates;
    private String operation;


    public Bank(String name, Currency[] rates, String operation) {
        this.name = name;
        this.rates = rates;
        this.operation = operation;

    }

    public boolean isSameAs(String bankName) {
        return name.equalsIgnoreCase(bankName);
    }

    public boolean isSameAsOperation(String operationName) {
        return operation.equalsIgnoreCase(operationName);
    }

    String prodaga = "Продажа";
    String pokupka = "Покупка";

    public void convert(String currency, int amount, String operation) {
        for (Currency currentRate : rates) {
            if (currentRate.getName().equalsIgnoreCase(currency) && operation.equalsIgnoreCase(prodaga)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / currentRate.getRate()));
            } else if (currentRate.getName().equalsIgnoreCase(currency) && operation.equalsIgnoreCase(pokupka)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в гривнах: %.2f", amount * currentRate.getRate()));
            }
        }
    }
}




