package com.company.TASK1.model;

public class BanksHolder {

    private Bank[] banks;

    public BanksHolder(Bank[] banks) {
        this.banks = banks;
    }

    public void calc(String bankName, String currency, int amount, String operation) {

        for (Bank currentBank : banks) {
            if (currentBank.isSameAs(bankName) && currentBank.isSameAsOperation(operation)) {
                currentBank.convert(currency, amount, operation);
            }
        }
    }
}


