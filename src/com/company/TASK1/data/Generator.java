package com.company.TASK1.data;

import com.company.TASK1.model.Bank;
import com.company.TASK1.model.Currency;

public final class Generator {

    public static final int BANKS_NUMBER = 3;

    private Generator() {
    }

    public static Bank[] generate() {
        Bank[] banks = new Bank[2 * BANKS_NUMBER];
        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 27.65f);
            currencies[1] = new Currency("eur", 29.86f);
            currencies[2] = new Currency("rub", 0.36f);
            banks[0] = new Bank("Приватбанк", currencies, "Продажа");

        }
        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 27.1f);
            currencies[1] = new Currency("eur", 29.2f);
            currencies[2] = new Currency("rub", 0.315f);
            banks[1] = new Bank("ПриватБанк", currencies, "Покупка");
        }
        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 27.95f);
            currencies[1] = new Currency("eur", 30.18f);
            currencies[2] = new Currency("rub", 0.375f);
            banks[2] = new Bank("Ощадбанк", currencies, "Продажа");

        }
        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 27.15f);
            currencies[1] = new Currency("eur", 29.0f);
            currencies[2] = new Currency("rub", 0.23f);
            banks[3] = new Bank("Ощадбанк", currencies, "Покупка");
        }
        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 27.75f);
            currencies[1] = new Currency("eur", 30.0f);
            currencies[2] = new Currency("rub", 0.355f);
            banks[4] = new Bank("ПУМБ", currencies, "Продажа");

        }
        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 27.25f);
            currencies[1] = new Currency("eur", 29.25f);
            currencies[2] = new Currency("rub", 0.335f);
            banks[5] = new Bank("ПУМБ", currencies, "Покупка");
        }

        return banks;

    }
}
