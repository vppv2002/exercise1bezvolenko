package com.company.TASK1;

import com.company.TASK1.data.Generator;
import com.company.TASK1.model.BanksHolder;

import java.util.Scanner;

public class Task1 {

    public void run() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите операцию (покупка или продажа): ");
        String operation = scan.nextLine();

        BanksHolder banksHolder = new BanksHolder(Generator.generate());

        System.out.println("Введите количесвто денег, которое хотите обменять: ");
        int amount = Integer.parseInt(scan.nextLine());

        System.out.println("Введите банк через который хотите совершить обмен (ПриватБанк, ОщадБанк или ПУМБ): ");
        String bank = scan.nextLine();

        System.out.println("Введите валюту (USD, EUR или RUB): ");
        String currency = scan.nextLine();

        banksHolder.calc(bank, currency, amount, operation);

    }
}
