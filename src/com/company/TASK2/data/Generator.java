package com.company.TASK2.data;

import com.company.TASK2.metod.Book;

public class Generator {

    public static final int numberOfBooks = 4;

    private Generator() {
    }

    public static Book[] generate() {
        Book[] books = new Book[numberOfBooks];
        books[0] = new Book();
        books[0].name = "Двадцать тысяч льё под водой";
        books[0].author = "Жюль Верн," + " Скотт Вальтер";
        books[0].yearOfCreation = 1869;
        books[0].edition = "Звезда";
        books[0].numberOfAuthor = 2;

        books[1] = new Book();
        books[1].name = "Великий Гэтсби";
        books[1].author = "Фрэнсис Скотт, " + "Александр Пушкин, " + "Скотт Фицджеральд," + " Saint Oliver";
        books[1].yearOfCreation = 1925;
        books[1].edition = "Зелёный огонёк";
        books[1].numberOfAuthor = 4;

        books[2] = new Book();
        books[2].name = "Евгений Онегин";
        books[2].author = "Александр Пушкин," + " Владимир Поспелов," + " Тарас Шевченко";
        books[2].yearOfCreation = 1823;
        books[2].edition = "Союз";
        books[2].numberOfAuthor = 3;

        books[3] = new Book();
        books[3].name = "Приключения Тома Сойера";
        books[3].author = "Марк Твен";
        books[3].yearOfCreation = 1876;
        books[3].edition = "Маркос";
        books[3].numberOfAuthor = 1;

        return books;
    }
}