package com.company.TASK2.metod;

public class BooksHolder {
    private Book[] books;

    public BooksHolder(Book[] books) {
        this.books = books;
    }

    public void calc(String authorOfBook, int numberOfBooks) {

        for (int i = 0; i < numberOfBooks; i++) {
            if (books[i].numberOfAuthor >= 3) {
                if (books[i].author.toUpperCase().contains(authorOfBook.toUpperCase())) {
                    System.out.println("Книга: " + books[i].name + ", Авторы: " + books[i].author + ", Год издания: " + books[i].yearOfCreation + ", Издательство: " + books[i].edition);

                }
            }
        }
    }
}