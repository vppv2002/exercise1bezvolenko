package com.company.TASK2;


import com.company.TASK2.metod.BooksHolder;
import com.company.TASK2.data.Generator;

import java.util.Scanner;


public class Task2 {


    public void run() {
        int numberOfBooks = 4;
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите имя автора(Жюль Верн, Фрэнсис Скотт, Скотт Фицджеральд, Александр Сергеевич Пушкин, Марк Твен, Saint Oliver, Скотт Вальтер, Владимир Поспелов, Тарас Шевченко): ");
        String authorOfBook = scan.nextLine();


        BooksHolder booksHolder = new BooksHolder(Generator.generate());

        booksHolder.calc(authorOfBook, numberOfBooks);
    }
}